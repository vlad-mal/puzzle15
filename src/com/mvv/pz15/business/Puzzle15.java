package com.mvv.pz15.business;

import java.io.PrintStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Puzzle15 {

	public static void ThatsAll() {
		println("Это всё.\r\n" + "\r\n" + "  (\\__/) \r\n" + " (='.'=) \r\n" + "('')_('')\r\n");
	}

	public Puzzle15() { // Конструктор
		init();
	};

	public boolean isAdmin; // Показывать с координатами и т.п.
	private int[] maze; // Позиция
	private long startGameMoment; // момент начала игры
	private long startStepMoment; // момент начала нового хода
	private int hole;
	private int steps;

	public void show() { // Показ пазла
		println("");
		if (isAdmin)
			println("   1  2  3  4");
		if (isAdmin)
			print(" ");
		println("+--+--+--+--+");
		for (int i = 0; i < maze.length; i++) {
			if ((i % 4) == 0) {// Начало строки
				if (isAdmin)
					printf("%d", i / 4 + 1);
				print("|");
			}

			if (maze[i] == 0) // Нолик показываем как пустой пазл
				print("  |");
			else
				printf("%2s|", maze[i]); // По два символа на пазл

			if ((i % 4) == 3) {
				println("");
				if (isAdmin)
					print(" ");
				println("+--+--+--+--+");
			}
		}
		if (!solvable())
			Puzzle15.println("--- Нерешаемо! ---");
		if (victory())
			Puzzle15.println("(Победа)");

	}

	public static <T> void println(T arg) { // Вывод строки с завершением
		System.out.println(arg);
	}

	public static <T> void print(T arg) { // Вывод строки без завершения
		System.out.print(arg);
	}

	public static PrintStream printf(String format, Object... args) { // Вывод по формату
		return System.out.printf(format, args);
	}

	public void init() { // Начальная инициализация
		maze = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0 };
		hole = 15;
		startGameMoment = System.currentTimeMillis();
		startStepMoment = System.currentTimeMillis();
		steps = 0;

	}

	public boolean initFromFile(File file) { // Начальная инициализация
		Scanner scf = null;
		try {
			scf = new Scanner(file);
			int[] tmpPzl = new int[16];
			for (int i = 0; i < maze.length; i++) {
				if (scf.hasNextInt())
					tmpPzl[i] = scf.nextInt();
				else {
					printf("*** Файл \"%s\" недопустимые данные: ==> ", file.getName());
					if (scf.hasNext())
						printf("%s\n", scf.next());
					else
						printf("Нет данных\n");
					tmpPzl = null;
					return false;
				}
				if (tmpPzl[i] == 0)
					hole = i;
			}

			maze = tmpPzl;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (scf != null)
				scf.close();
		}
		return true;
	}

	public void shuffle(int times) {
		while (times-- > 0)
			shake();
	}

	public void exchange(int xy_i, int xy_j) {
		// Сменаместами пазлов
		// Параметры - координаты, нумерация - от 1 до 4.
		// Например: (41, 22) => менять местами элементь с координатами (4,1) с
		// элементом (2,4)
		int x_i = xy_i / 10;
		int y_i = xy_i % 10;
		int x_j = xy_j / 10;
		int y_j = xy_j % 10;
		if ((x_i >= 1) & (x_i <= 4) & (y_i >= 1) & (y_i <= 4) & (x_j >= 1) & (x_j <= 4) & (y_j >= 1) & (y_j <= 4))
			swap(--x_i * 4 + --y_i, --x_j * 4 + --y_j);
	}

	private void swap(int i, int j) {
		maze[i] ^= maze[j];
		maze[j] ^= maze[i];
		maze[i] ^= maze[j];
	}

	private void shake() {
		while (true) {
			switch (ThreadLocalRandom.current().nextInt(4)) {
			case 0:
				if ((hole % 4) != 0) // Влево.
				{ // Дырка не в левом сталбце
					swap(hole - 1, hole);
					hole--;
					return;
				}
			case 1:
				if ((hole % 4) != 3) // Вправо
				{ // Дырка не в правом столбце
					swap(hole + 1, hole);
					hole++;
					return;
				}
			case 2:
				if ((hole - 4) >= 0) // Вверх
				{ // Дырка не в верхнем ряду
					swap(hole - 4, hole);
					hole -= 4;
					return;
				}
			case 3:
				if ((hole + 4) <= 15) // Вниз
				{ // Дырка не в нижнем ряду
					swap(hole + 4, hole);
					hole += 4;
					return;
				}
			}
		}
	}

	public boolean victory() // Если кубики сложены по порядку - это победа!
	{
		for (int i = 0; i < 15; i++)
			if (maze[i] != (i + 1))
				return false;
		return true;
	}

	public boolean solvable() { // Решаема ли текущая комбинация?
		// Доказательтво зело сложно:
		// http://www.cs.cmu.edu/afs/cs/academic/class/15859-f01/www/notes/15-puzzle.pdf
		// Суть:
		// Пусть дана некоторая позиция на доске a_1..a_16, где один из элементов равен
		// нулю ("дырка").
		// Рассмотрим перестановку:
		// a_1 a_2 ... a_(z-1) a_(z+1) ... a_15 a_16
		// т.е. перестановку всех чисел соответствующих позиции на доске, без нулевого
		// элемента
		// Обозначим через N количество инверсий в этой перестановке
		// (т.е. количество таких элементов a_i и a_j, что i < j, но a_i > a_j).
		// Далее, пусть K — номер строки, в которой находится пустой элемент
		// (т.е. в наших обозначениях K = (z-1) % 4 + 1).
		// Тогда, решение существует тогда и только тогда, когда N+K чётно.
		//

		int inv = 0; // Число инверсий
		for (int i = 0; i < 16; ++i)
			if (maze[i] > 0) // Не - дырка
				for (int j = 0; j < i; ++j)
					if (maze[j] > maze[i])
						++inv;
		// Число инверсий N подсчитано, теперь надо добавить номер строки K с дыркой
		for (int i = 0; i < 16; ++i) // Ищем строку, в которой дырка
			if (maze[i] == 0) {
				inv += 1 + i / 4; // Добавляем номер строки (N+K)
				break;
			}
		return inv % 2 == 0; // Решение есть, если N+k четно
	}

	public void left() {// "Влево"
		if ((hole % 4) != 3) // Пазл влево, дырку вправо
		{ // Если дырка не крайняя правая
			swap(hole + 1, hole);
			hole++;
			if (!isAdmin)
				step();
		}
	}

	@Override
	public String toString() {
		String rez = "";
		for (int i : maze)
			rez = rez + " " + i;
		return rez;
	}

	private void step() {

		show();
		if (isAdmin)
			return;
		long totalTime = System.currentTimeMillis() - startGameMoment;
		long totalStepTime = System.currentTimeMillis() - startStepMoment;
		printf("Время всего :%4.2f сек. Время на ход: %4.2f сек. Ходов: %d \n", totalTime / 1000.0,
				totalStepTime / 1000.0, ++steps);
		startStepMoment = System.currentTimeMillis();

	}

	public void right() { // "Вправо"
		if (hole % 4 != 0) // Пазл вправо, дырку влево
		{ // Если дырка не крайняя левая
			swap(hole - 1, hole);
			hole--;
			if (!isAdmin)
				step();
		}
	}

	public void up() { // "Вверх"

		if ((hole + 4) <= 15) // Пазл вверх, дырку вниз
		{ // Если дырка не на самом нижнем ряду
			swap(hole + 4, hole);
			hole += 4;
			if (!isAdmin)
				step();
		}
	}

	public void down() { // Стрелка "вниз"

		if ((hole - 4) >= 0) // Пазл вниз, дырку вверх
		{ // Если дырка не на самом верхнем ряду
			swap(hole - 4, hole);
			hole -= 4;
			if (!isAdmin)
				step();
		}
	}

}
