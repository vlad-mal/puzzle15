package com.mvv.pz15.admin;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

import com.mvv.pz15.business.Puzzle15;

public class Admin {

	private static Scanner scanner = null;
	private static Puzzle15 puzzle;

	public static void main(String[] args) {
		System.out.println("ПЗ слушателя Малиновского В.В. \"Пятнашки\".");
		System.out.println("Администратор.");
		puzzle = new Puzzle15();
		puzzle.isAdmin = true;
		try {
			scanner = new Scanner(System.in);
			edit();

		} finally {
			if (scanner != null)
				scanner.close();
		}
		Puzzle15.ThatsAll();
	}

	private static void loadFromFile() {
		// Собираем список файлов *.pzp

		File folder = new File(System.getProperty("user.dir") + "\\data");
		while (true) {
			// Показ списка файлов
			File[] arrFiles = folder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File testedFile) {
					return testedFile.isFile() & testedFile.getName().endsWith(".pzp");
				}
			});
			if (arrFiles.length == 0) {
				System.out.println("Файлов исходных позиций (.pzp) не найдено.");
				return;
			}

			System.out.println("Файлы исходных позиций:");
			for (int i = 0; i < arrFiles.length; i++)
				System.out.printf("Файл %2d:  %s\n", i + 1, arrFiles[i].getName());
			System.out.println("Введите номер файла для загрузки. Y - отказ выбора");

			String str = scanner.nextLine().toLowerCase();
			switch (str) {
			case "y":
				return; // Выход
			default: {
				try {
					int fNum = Integer.valueOf(str);
					if ((fNum < 1) || (fNum > arrFiles.length))
						continue;
					if (puzzle.initFromFile(arrFiles[fNum - 1]))
						return;
					continue;
				} catch (NumberFormatException e) {
					System.out.printf("\"%s\" - Недопустимое значение для целого\n", str);
					continue;
				}

			}
			}
		}

	}

	private static void edit() {

		while (true) {
			puzzle.show();
			System.out.println("a - влево, d - вправо, w - верх, s - вниз, ab cd - поменять местами (a, b) <-> (c, d)");
			System.out.println("Y - выход, 0 - сброс, 1 - потрясти, 2 - загрузить, 3 - сохранить.");

			String str = scanner.nextLine().toLowerCase();
			switch (str) {
			case "y":
				return;
			case "0":
				puzzle.init();
				break;
			case "2":
				loadFromFile();
				break;
			case "3":
				saveToFile();
				break;
			case "1":
				puzzle.shuffle(5);
				break;
			case "a":
				puzzle.left();
				break;
			case "d":
				puzzle.right();
				break;
			case "w":
				puzzle.up();
				break;
			case "s":
				puzzle.down();
				break;
			default: {
				if (! str.contains(" "))
					break;
				Scanner sci = new Scanner(str);
				try {
					int xy_i = sci.nextInt();
					int xy_j = sci.nextInt();
					puzzle.exchange(xy_i, xy_j);
				} catch (Exception e) {
					System.out.println("Фигня-с:" + str);
				} 
				finally {
					sci.close();
				}
			}
			}

		}
	}

	private static void saveToFile() {
		while (true) {
			System.out.print("Введите имя файла (пустая строка - отмена):");
			String fn = scanner.nextLine();
			if (fn.length() == 0)
				return;
			if (!fn.contains("."))
				fn = fn + ".pzp";

			OutputStream os = null;
			try {
				os = new FileOutputStream(new File(System.getProperty("user.dir") + "\\data\\" + fn));
				os.write(puzzle.toString().getBytes(), 0, puzzle.toString().getBytes().length);
				return;
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (os != null)
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
		}

	}
}
