package com.mvv.pz15.game;

import java.io.File;
import java.io.FileFilter;
import java.util.Scanner;

// 
import com.mvv.pz15.business.Puzzle15;

public class Game {
	private static Scanner scanner = null;
	private static Puzzle15 puzzle;

	public static void main(String[] args) {
		System.out.println("ПЗ слушателя Малиновского В.В. \"Пятнашки\".");
		System.out.println("Игра.");
		puzzle = new Puzzle15();
		try {
			scanner = new Scanner(System.in);
			while (takeInit())
				play();

		} finally {
			if (scanner != null)
				scanner.close();
		}
		Puzzle15.ThatsAll();
	}
	

	private static boolean takeInit() {
		// Собираем список файлов *.pzp

		File folder = new File(System.getProperty("user.dir") + "\\data");
		while (true) {
			// Показ списка файлов
			File[] arrFiles = folder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File testedFile) {
					return testedFile.isFile() & testedFile.getName().endsWith(".pzp");
				}
			});
			if (arrFiles.length > 0) {
				System.out.println("Файлы исходных позиций:");
				for (int i = 0; i < arrFiles.length; i++)
					System.out.printf("Файл %2d:  %s\n", i + 1, arrFiles[i].getName());
				System.out.println("Введите номер файла для загрузки.");
			} else
				System.out.println("Файлов исходных позиций (.pzp) не найдено.");
			System.out.println("0 - иницализация случайными значениями. Y - выход");

			String str = scanner.nextLine().toLowerCase();
			switch (str) {
			case "y":
				return false; // Выход
			case "0":
				puzzle.init();
				puzzle.shuffle(5);
				return true;
			default: {
				try {
					int fNum = Integer.valueOf(str);
					if ((fNum < 1) || (fNum > arrFiles.length))
						continue;
					if (puzzle.initFromFile(arrFiles[fNum - 1]))
						return true;
					continue;
				} catch (NumberFormatException e) {
					System.out.printf("\"%s\" - Недопустимое значение для целого\n", str);
					continue;
				}

			}
			}
		}

	}

	private static void play() {
		puzzle.show();
		if (! puzzle.solvable()) {
			Puzzle15.println("--- Нерешаемо! ---");
		}

		while (true) {
			String str = scanner.nextLine().toLowerCase();
			switch (str) {
			case "y":
				return;
			case "a":
				puzzle.left();
				break;
			case "d":
				puzzle.right();
				break;
			case "w":
				puzzle.up();
				break;
			case "s":
				puzzle.down();
				break;
			}
			
			if (puzzle.victory()) {
				Puzzle15.println("--- Победа! ---");
				Puzzle15.println("---------------");
				break;
			}
		}
	}
}

